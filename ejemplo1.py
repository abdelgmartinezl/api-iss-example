import requests
import time

# Llamado correcto a un API endpoint, sin parametros
resp = requests.get("http://api.open-notify.org/astros.json")
print(resp.status_code)
data = resp.json()
#print(data)
print("Actualmente, el ISS tiene " + str(data['number']) + " personas.")

# Llamado correcto a un API endpoint, con parametros
p = {"lat": 8.9936, "lon": -79.5201}
resp = requests.get("http://api.open-notify.org/iss-pass.json", params=p)
data = resp.json()
#print(data)
pases = data['response']
print("A continuacion, las veces que el ISS pasara sobre Panamá:")
for pase in pases:
   print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(pase['risetime'])))

# Llamado incorrecto a un API endpoint
#resp = requests.get("http://api.open-notify.org/astro.json")
#print(resp.status_code)

